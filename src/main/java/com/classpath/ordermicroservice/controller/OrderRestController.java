package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.command.OrderProcessingCommand;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Slf4j
public class OrderRestController {

    private final OrderService orderService;
    private final OrderProcessingCommand orderProcessingCommand;

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
      log.info("Inside the save order method of controller :: {} ", order);
      order.getLineItems().forEach(lineItem -> lineItem.setOrder(order));
      return this.orderProcessingCommand.createOrder(order);
    }

    @GetMapping
    public Map<String, Object> fetchOrders(
            @RequestParam(value = "pageno", defaultValue = "0") int pageNo,
            @RequestParam(value = "records", defaultValue = "10") int records,
            @RequestParam(value = "sort", defaultValue = "orderDate") String sortBy) {

        return this.orderService.fetchAll(pageNo,records,sortBy );
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.fetchById(orderId);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrderByid(@PathVariable("id") long orderId){
        this.orderProcessingCommand.cancelOrder(orderId);
        this.orderService.deleteById(orderId);
    }

    @DeleteMapping
    public void deleteAllOrders(){
        this.orderService.deleteAllOrders();
    }
}