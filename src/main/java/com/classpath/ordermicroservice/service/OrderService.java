package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.model.PaymentStatus;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.tomcat.jni.Local;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;

    @CircuitBreaker(name = "paymentmicroservice")
    @Retry(name="paymentmicroservice", fallbackMethod = "fallbackOrder")
    public Order saveOrder(Order order){
        log.info("Saving the order :: {}", order);
        log.info(" Retrying call to payment microservice :: " );
      //  final ResponseEntity<PaymentStatus> responseEntity = this.restTemplate.postForEntity("http://<service-name>/api/payments/22", order, PaymentStatus.class);
       // log.info("Posted the inventory :: {}", responseEntity.getBody());
        Order savedOrder = this.orderRepository.save(order);
        return  savedOrder;
    }

    private Order fallbackOrder(Exception exception){
        log.error("Exception while updating the payment:: {}", exception.getMessage());
        return Order.builder().orderDate(LocalDate.now()).orderId(1111).build();
    }

    public Map<String, Object> fetchAll(int pageNo, int records, String sortBy){
        // Java 8 - return new HashSet<>(this.orderRepository.findAll());
        // Java 11 -
         //return Set.copyOf(this.orderRepository.findAll());
        var pageRequest = PageRequest.of(pageNo, records, Sort.by(sortBy));
        var pageResult = this.orderRepository.findAll(pageRequest);
        val totalRecords = pageResult.getTotalElements();
        final int totalPages = pageResult.getTotalPages();
        final List<Order> content = pageResult.getContent();

        val result = new LinkedHashMap<String, Object>();
        result.put("totalRecords", totalRecords);
        result.put("totalPages", totalPages);
        result.put("data", content);
        return result;
    }

    public Order fetchById(long orderId){
        log.info("Fetching the order by Id :: {}", orderId);

        return this.orderRepository
                        .findById(orderId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid Order id"));
    }

    public Order updateOrder(Order order){
        final Optional<Order> optionalOrder = this.orderRepository.findById(order.getOrderId())
                .map(o -> this.mergeOrder(o, order));
        if(optionalOrder.isPresent()){
            final Order toBeSavedOrder = optionalOrder.get();
            return orderRepository.save(toBeSavedOrder);
        }
        return order;
    }

    private Order mergeOrder(Order fetchedOrder, Order updatedOrder) {
        fetchedOrder.setOrderDate(updatedOrder.getOrderDate());
        fetchedOrder.setLineItems(updatedOrder.getLineItems());
        fetchedOrder.setCustomerName(updatedOrder.getCustomerName());
        fetchedOrder.setPrice(updatedOrder.getPrice());
        return fetchedOrder;
    }

    public void deleteById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public void deleteAllOrders() {
        log.warn("About to delete all the orders :: ");
        this.orderRepository.deleteAll();
    }
}