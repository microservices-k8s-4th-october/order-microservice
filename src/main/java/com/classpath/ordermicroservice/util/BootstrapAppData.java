package com.classpath.ordermicroservice.util;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import static java.time.ZoneId.systemDefault;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.stream.IntStream.range;

@Configuration
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;
    private final Faker faker;
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        range(1, 1000)
                    .forEach(index ->  {
                        Order order = Order
                                .builder()
                                .orderDate(faker.date().past(4, DAYS).toInstant().atZone(systemDefault()).toLocalDate())
                                .customerName(faker.name().firstName())
                                .emailAddress(faker.internet().emailAddress())
                                .price(faker.number().randomDouble(2, 25000, 50000))
                                .build();
                        range(1,4).forEach(val -> {
                            order.addLineItem(LineItem.builder().price(faker.number().randomDouble(2, 1000, 2500)).qty(faker.number().numberBetween(2,10)).build());
                        });
                        this.orderRepository.save(order);
        });
    }
}