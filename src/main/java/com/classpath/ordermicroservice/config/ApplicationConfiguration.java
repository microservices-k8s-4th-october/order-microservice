package com.classpath.ordermicroservice.config;

import com.github.javafaker.Faker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public Faker faker(){
        return new Faker();
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}