package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class ApplicationDBHealthEndpoint implements HealthIndicator {

    private final OrderRepository orderRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public Health health() {
        //perform health check
        long numberOfOrders = this.orderRepository.count();
        if (numberOfOrders > 0 ){
       //     AvailabilityChangeEvent.publish(this.applicationEventPublisher, null, LivenessState.CORRECT);
            return Health.up().withDetail("DB Service", "DB service is up").build();
        } else {
         //   AvailabilityChangeEvent.publish(this.applicationEventPublisher, null, LivenessState.BROKEN);
            return Health.status(Status.DOWN).withDetail("DB sevice", "DB service is down").build();
        }
    }
}
@Component
class ApplicationKafkaHealthEndpoint implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("Kafka Service", "Kafka service is up").build();
    }
}
@Component
class PaymentGatewayHealthEndpoint implements HealthIndicator{

    @Override
    public Health health() {
        return Health.up().withDetail("PaymentGateway Service", "PaymentGateway service is up").build();
    }
}