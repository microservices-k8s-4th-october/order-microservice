package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public final class OrderEvent {

    private OrderEventType eventType = OrderEventType.PENDING;
    private final Order order;
    private final LocalDateTime localDateTime;
}