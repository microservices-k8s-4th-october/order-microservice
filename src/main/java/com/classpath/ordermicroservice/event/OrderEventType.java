package com.classpath.ordermicroservice.event;

import org.springframework.stereotype.Component;

public enum OrderEventType {
    PENDING,
    CREATED,
    CANCELLED
}
