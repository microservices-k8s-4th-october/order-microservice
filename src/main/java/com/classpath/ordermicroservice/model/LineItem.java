package com.classpath.ordermicroservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Data
@EqualsAndHashCode(exclude = "order")
@ToString(exclude = "order")
@Entity
@Table(name="line_items")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int lineItemId;

    private int qty;

    private double price;

    @ManyToOne
    @JoinColumn(name="order_id", nullable = false)
    @JsonIgnore
    private Order order;
}