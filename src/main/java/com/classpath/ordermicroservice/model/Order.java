package com.classpath.ordermicroservice.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.AUTO;

@Data
@EqualsAndHashCode(of = {"orderId", "customerName", "orderDate"})
@Entity
@Table(name="orders")
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @NotEmpty(message = "Customer name cannot be empty")
    private String customerName;

    @Min(value = 2000, message = "Order price should be of min 2k")
    @Max(value = 50000, message = "Order price can be of max 50k")
    private double price;

    @PastOrPresent
    private LocalDate orderDate;

    private String emailAddress;

    @OneToMany(mappedBy = "order", cascade = ALL, fetch = EAGER)
    @NotNull(message = "Order should contain line items")
    private Set<LineItem> lineItems;

    //scaffolding code
    public void addLineItem(LineItem lineItem){
        if ( this.lineItems == null ){
            this.lineItems = new HashSet<>();
        }
        this.getLineItems().add(lineItem);
        lineItem.setOrder(this);
    }
}