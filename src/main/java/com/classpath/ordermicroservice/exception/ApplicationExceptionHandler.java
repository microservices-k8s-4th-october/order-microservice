package com.classpath.ordermicroservice.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Component
@Slf4j
public class ApplicationExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidOrder(Exception exception){
        log.error("Exception with invalid order id :: {}", exception.getMessage());
        return ResponseEntity.status(NOT_FOUND).body(new Error(100, exception.getMessage()));
    }
}

@NoArgsConstructor
@AllArgsConstructor
@Data
class Error {
    private int code;
    private String message;
}