package com.classpath.ordermicroservice.command;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import static com.classpath.ordermicroservice.event.OrderEventType.CANCELLED;
import static com.classpath.ordermicroservice.event.OrderEventType.CREATED;
import static java.time.LocalDateTime.now;

@Component
@Slf4j
@AllArgsConstructor
public class OrderProcessingCommand {

    private OrderService orderService;
    private KafkaTemplate<Long, OrderEvent> kafkaTemplate;
    public final static String TOPIC_NAME = "order-pradeep-new-topic";

    public Order createOrder(Order order){
      log.info("Processing the creation of order :: {} ", order);
        Order savedOrder = this.orderService.saveOrder(order);
        //create a order created event
        OrderEvent orderEvent = OrderEvent.builder().eventType(CREATED).order(savedOrder).localDateTime(now()).build();
        log.info("Order created event :: {}", orderEvent);
        //save this event to the kafka topic
        this.kafkaTemplate.send(TOPIC_NAME, orderEvent);
        return savedOrder;
    }

    public void cancelOrder(long orderId) {
        log.info("Processing the cancellation of order :: {} ", orderId);
        final Order savedOrder = this.orderService.fetchById(orderId);
        this.orderService.deleteById(orderId);
        OrderEvent event = OrderEvent.builder().eventType(CANCELLED).order(savedOrder).localDateTime(now()).build();
        log.info("Order cancelled event :: {} ", event);
        //pushing the order cancelled event to the kafka topic
        log.info("Order created event :: {}", event);
        //save this event to the kafka topic
        this.kafkaTemplate.send(TOPIC_NAME, event);

    }
}