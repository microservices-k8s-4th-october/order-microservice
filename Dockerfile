# standard instructions
FROM openjdk:11 as builder
# stage 1
WORKDIR /app
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
RUN chmod +x ./mvnw
RUN ./mvnw -B dependency:go-offline

# stage 2
COPY src src
RUN ./mvnw clean package -DskipTests
RUN mkdir -p target/dependency && (cd target/dependency; jar -xvf ../*.jar)

# stage 3

FROM openjdk:11.0.12-jre-slim-buster as stage

ARG DEPENDENCY=/app/target/dependency

# Copy the dependency application file from builder stage artifact
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

EXPOSE 8222

ENTRYPOINT ["java", "-cp", "app:app/lib/*", "com.classpath.ordermicroservice.OrderMicroserviceApplication"]






